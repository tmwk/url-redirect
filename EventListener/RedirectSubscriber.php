<?php
/*
 * ************************************************************************
 *  * Nombre del Archivo: RedirectListener.php
 *  * Autor: Mario Figueroa [mfigueroa@tmwk.cl]
 *  * Fecha de Creación: 2/8/23 16:54
 *  ***********************************************************************
 *  * Copyright (c) 2023 Mario Figueroa
 *  * Queda prohibida la distribución y uso no autorizado de este archivo.
 *  * Para obtener más detalles, consulta el archivo LICENSE.md
 *  ***********************************************************************
 */

namespace TMWK\RedirectBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use TMWK\RedirectBundle\Entity\Redirect;

final class RedirectSubscriber implements EventSubscriberInterface
{
    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {

    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [
                ['processException', 10],
            ],
        ];
    }

    public function processException(ExceptionEvent $event): RedirectResponse|false
    {
        $exception = $event->getThrowable();

        if ($exception instanceof HttpExceptionInterface) {
            if ($exception->getStatusCode() == 404) {
                
                $current_url = $event->getRequest()->server->get('REQUEST_URI');
                $redirect    = $this->entityManager->getRepository(Redirect::class)->findOneBy(array('url_old' => $current_url));

                if ($redirect) {
                    $response = new RedirectResponse($redirect->getUrlNew(), 302);
                    $response->send();
                }

            }
        }
        return false;
    }
}