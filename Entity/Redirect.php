<?php
/*
 * ************************************************************************
 *  * Nombre del Archivo: Redirect.php
 *  * Autor: Mario Figueroa [mfigueroa@tmwk.cl]
 *  * Fecha de Creación: 2/8/23 14:42
 *  ***********************************************************************
 *  * Copyright (c) 2023 Mario Figueroa
 *  * Queda prohibida la distribución y uso no autorizado de este archivo.
 *  * Para obtener más detalles, consulta el archivo LICENSE.md
 *  ***********************************************************************
 */

namespace TMWK\RedirectBundle\Entity;

use DateTime;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Entity
 * @ORM\Table(name="redirect")
 */
class Redirect
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank(message="El campo no puede quedar vacio")
     * @var string $url_old
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $url_old;

    /**
     * @Assert\NotBlank(message="El campo no puede quedar vacio")
     * @var string $url_new
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $url_new;

    /**
     * @Gedmo\SortablePosition
     * @var string $position
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $position;

    /**
    * @var DateTime $create_at
    *
    * @Gedmo\Timestampable(on="create")
    * @ORM\Column(type="datetime", nullable=true)
    */
    protected $create_at;

    /**
    * @var DateTime $update_at
    *
    * @Gedmo\Timestampable(on="update")
    * @ORM\Column(type="datetime", nullable=true)
    */
    protected $update_at;


    public function __toString()
    {
        return $this->getUrlOld();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrlOld(): ?string
    {
        return $this->url_old;
    }

    public function setUrlOld(?string $url_old): static
    {
        $this->url_old = $url_old;

        return $this;
    }

    public function getUrlNew(): ?string
    {
        return $this->url_new;
    }

    public function setUrlNew(?string $url_new): static
    {
        $this->url_new = $url_new;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): static
    {
        $this->position = $position;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->create_at;
    }

    public function setCreateAt(?\DateTimeInterface $create_at): static
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->update_at;
    }

    public function setUpdateAt(?\DateTimeInterface $update_at): static
    {
        $this->update_at = $update_at;

        return $this;
    }


}
