<?php

namespace TMWK\RedirectBundle\Admin;

use Runroom\SortableBehaviorBundle\Admin\SortableAdminTrait;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

final class RedirectAdmin extends AbstractAdmin
{
//    use SortableAdminTrait;
    protected $baseRouteName    = 'redirect';
    protected $baseRoutePattern = 'redirect';

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('url_old', null, ['label' => 'URL Anterior'])
            ->add('url_new', null, ['label' => 'URL Nueva']);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('url_old', null, ['label' => 'URL Anterior'])
            ->add('url_new', null, ['label' => 'URL Nueva']);
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => '#', 'header_style' => 'width: 1%;'))
            ->addIdentifier('url_old', null, ['label' => 'URL Anterior'])
            ->addIdentifier('url_new', null, ['label' => 'URL Nueva'])
            ->add(ListMapper::NAME_ACTIONS, ListMapper::TYPE_ACTIONS, [
                'translation_domain' => 'SonataAdminBundle',
                'actions'            => [
                    'show' => [],
                    'edit' => [],
                    /*'move' => [
                        'template' => '@RunroomSortableBehavior/sort.html.twig',
                        'enable_top_bottom_buttons' => false,
                    ],*/
                ],
            ])
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('url_old', null, ['label' => 'URL Anterior'])
            ->add('url_new', null, ['label' => 'URL Nueva']);
    }

}